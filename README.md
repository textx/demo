http://textx.github.io/textX/3.0/tutorials/entity/

Prerequisites:
The environment should have the graphviz installed as well as the python requirements. To install graphviz, run `sudo apt install graphviz`.

To create and activate the virtaul env, run `virtualenv demo` and `source demo/bin/activate`

To generate the meta model and model files, run `textx generate entity.tx --target dot` and `textx generate person.ent --grammar entity.tx --target dot`

To create png files
    `dot -Tpng -O *.dot`

To parse the model alone, `python model_parser.py`

To generate code, run `python code_generator.py`