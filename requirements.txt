Arpeggio==2.0.0
click==7.1.2
future==0.18.2
Jinja2==3.1.2
MarkupSafe==2.1.1
textX==3.0.0
xdot==1.2
