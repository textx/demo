from textx.metamodel import metamodel_from_file

metamodel=metamodel_from_file('entity.tx')
model=metamodel.model_from_file('person.ent')

print(type(model))
print(model.__class__.__name__)
print(model.simple_types)
print(model.entities)
print(model.entities[0].properties)
print(model.entities[0].properties[0].name)
print(model.entities[0].properties[0].type)
print(model.entities[0].properties[1].name)
print(model.entities[0].properties[1].type)
print(model.simple_types[0].name)
# print(model.simple_types[0].type)  -- no type specified yet
print(model.simple_types[1].name)
# print(model.simple_types[1].type)  -- no type specified yet